from distutils.core import setup

setup(
    name         = 'biofortis-labmatrixclient',
    version      = '1.0',
    description  = 'Python wrapper for the Labmatrix SOAP API',
    author       = 'James McIninch',
    author_email = 'james.mcininch@biogen.com',
    url          = 'https://bitbucket.org/biogenidec/biofortis-labmatrixclient-python',
    license      = 'BSD',
    py_modules=['BioFortis']
)
