# BioFortis.py

This module implements a Python class, LabmatrixClient, that is a wrapper to the
Labmatrix SOAP web services as documented here:

  https://biofortis.trac.cvsdude.com/API/wiki/WebServices5.3

Example:


```
#!python

import BioFortis

lm = BioFortis.LabmatrixClient('http://labmatrix.mydomain.com')

if lm.login('username','password'):
   result = lm.executeQuery(54)
   print "\t".join( [ header.fieldName for header in response.queryField ] )
   for record in lm.getResultIterator(result.id):
       print "\t".join( [ field.value for field in record.fieldValue ] )
```


## INSTALLATION

    $ python setup.py install

or

    $ mkdir -p $HOME/lib/python
    $ python setup.py install --prefix=$HOME/lib/python

## NOTE

I no longer have a Labmatrix instance against which I can develop or test this module, so I am looking for an interested party to take over maintaining this repository and module. If you
are interested, please e-mail me at james.mcininch @ biogen.com