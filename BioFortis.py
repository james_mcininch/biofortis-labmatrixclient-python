#!/usr/bin/python

# Copyright (c) 2015, James McIninch <james.mcininch@biogen.com> and Biogen, Inc.
# 225 Binney St., Cambridge, MA 02142, USA
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted
# provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright notice, this list of
#       conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright notice, this list of
#       conditions and the following disclaimer in the documentation and/or other materials
#       provided with the distribution.
#
#    3. Neither the name of the copyright holder nor the names of its contributors may be used to
#       endorse or promote products derived from this software without specific prior written
#       permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


'''BioFortis.py

This module implements a Python class, LabmatrixClient, that is a wrapper to the
Labmatrix SOAP web services as documented here:

  https://biofortis.trac.cvsdude.com/API/wiki/WebServices5.3

Example:
  import BioFortis
  lm = BioFortis.LabmatrixClient('http://labmatrix.mydomain.com')
  if lm.login('username','password'):
      result = lm.executeQuery(54)
      print "\t".join( [ header.fieldName for header in response.queryField ] )
      for record in lm.getResultIterator(result.id):
          print "\t".join( [ field.value for field in record.fieldValue ] )
'''

from os.path import expanduser
from SOAPpy import Config, SOAPProxy

import re
import signal
import sys

# Config.debug = 1

class LabmatrixClient:
    '''A Labmatrix SOAP client proxy class.
    
    There are no public attributes for this class. 
    '''
    
    _PROXY = 'labmatrix/jsp/ws'
    _NS    = 'http://labmatrix.net/schemas'
    
    def __init__(self, url='http://labmatrix.local', cookiejar='~/.lmcookies'):
        '''__init__(url, cookiejar)
        
        Initialize the LabmatrixClient class.
        
        Args:
          url (str, optional) : the root URL of the Labmatrix application (generally, the FQDN)
          cookiejar (str, optional) : path to a file for storing cookie data (not used)
        '''
        
        self._baseurl = url
        self._cookiejar = expanduser(cookiejar)
        self._proxy = '/'.join([url, LabmatrixClient._PROXY])
        self._server = SOAPProxy(self._proxy)._ns(LabmatrixClient._NS)
        
        # self._server.config.debug               = 1
        # self._server.config.strictNamespaces    = 1
        
    def ping(self):
        '''ping()
        
        This function tests to make sure that it can call the SOAP web service.
        
        Returns:
          True if successful, otherwise false.
        '''
        
        message = "Testing... Testing... 1... 2... 3..."
        response = self._server.PingRequest( message = message )
        return response == message
        
    def login(self, username, password, timeoutInMinutes = 5):
        '''login(username, password, timeoutInMinutes = 5)
        
        Send credentials to the Labmatrix SOAP web service to request a token that can be used to
        authenticate later API calls. Note that this stores the session token in the LabmatrixClient
        object instance so that the user need not provide it with subsequent calls. This method
        also stores the username and password in plain-text in the LabmatrixClient instance so that
        it can login again if the SOAP API calls time-out (a serious issue with retrieveResults).
        
        Args:
          username (str) : the Labmatrix username used to authenticate to the service
          password (str) : the Labmatrix password used to authenticate to the service
          timeoutInMinutes (int, optional) : how long the  web session will stay alive before
            it terminates itself. The default is 5 minutes.
            
        Result:
          True if login is successful, otherwise False.
        '''

        print 'login({0}, ********, {1})'.format(username, timeoutInMinutes)
        
        self._timeout  = timeoutInMinutes
        self._username = username
        self._password = password
        
        result = self._server.LoginRequest(username = username, password = password, timeoutInMinutes = timeoutInMinutes)
        
        if result == 'Login failed.' :
            if hasattr(self, '_username'): del(self._username) 
            if hasattr(self, '_password'): del(self._password)
            if hasattr(self, '_sessionToken'): del(self._sessiontToken)
            return False
        else:
            self._username     = username
            self._password     = password
            self._sessionToken = result
            return True
    
    def logout(self):
        '''logout()
        
        This call will release the Labmatrix session and any current queries.
        '''
        print 'logout()'
        self._server.LogoutRequest(sessionToken = self._sessionToken)
        
    def listRunnableQueries(self):
        '''listRunnableQueries()
        
        Provide a list of Runnable / Stored queries with the names and query ID numbers.
        These queries are listed in the application's Qiagram interface in the left-most panel.
        
        Returns:
           [ { 'queryId': 'queryId1', 'queryName': 'queryName1' } ... ]
        '''
        
        return self._server.ListRunnableQueriesRequest(sessionToken = self._sessionToken)
        
    def listVariablesForQuery(self, queryId = None, queryName = None):
        '''listVariablesForQuery(queryId, queryName)
        
        Provides a list of variables that a parameterized query requires. Many queries may have
        no user parameters. The argument queryId OR queryName must be provided.
        
        Args (at least one of queryId or queryName is required):
          queryId (int, optional) : the numeric identifier for the query
          queryName (str, optional) : the string used as the query's name
          
        Returns:
          [ { 'caption': 'caption1', 'helpText': 'helpText1,
              'operator': 'operator1', 'operatorType': 'operatorType1'} ... ]
        '''
        
        if queryId is not None and not re.match(r'^\d+$', str(queryId)):
            queryName = queryId
            queryId = None
        
        if queryId is not None:
            return self._server.ListVariablesForQueryRequest(sessionToken = self._sessionToken, queryId = queryId)
        else:
            return self._server.ListVariablesForQueryRequest(sessionToken = self._sessionToken, queryName = queryName)
            
    def executeQuery(self, queryId = None, queryName = None, **queryArgs):
        '''executeQuery(queryId, queryName, queryArgs)
        
        This requests Labmatrix to perform a query by ID or name. These queries are the saved
        queries that are stored in the Qiagram portion of the application. Caveat: queries that
        return many rows may not function properly with the SOAP interface, and the performance
        of the SOAP interface is VERY SLOW. For large queries, defer to the REST method
        (DownloadQueryResults).
        
        This function returns a object with attributes that correspond directly to the SOAP
        response. The response ID ('id') is used with retrieveResults() to get the records.
        The 'queryField' attribute holds a list of fields and their labels (suitable to use
        as column headers / field labels).
        
        Args (at least one of queryId or queryName is required):
          queryId (int, optional) : the numeric identifier for the query
          queryName (int, optional) : the string used as the query's name
          queryArgs (optional) : a list of argument names and values as required (applies only
            to queries that require variables)
            
        Returns:
          [ 'id' : resultId, 'queryField' : [ { 'fieldKey': 'key1', 'fieldName': 'name1',
            'setName': 'set1', 'displayFieldKey': 'display1' }, ... ] ]
        '''
        
        print 'executeQuery({0}, {1}, {2})'.format(queryId, queryName, queryArgs)
        
        if queryId is not None and not re.match(r'^\d+$', str(queryId)):
            queryName = queryId
            queryId = None
                
        if queryId is not None:
            tmp = self._server.ExecuteQueryRequest(sessionToken = self._sessionToken, queryId = queryId, variable = queryArgs)
        else:
            tmp = self._server.ExecuteQueryRequest(sessionToken = self._sessionToken, queryName = queryName, variable = queryArgs)
            
        class ExecuteQueryResponse:
            pass
        
        class ExecuteQueryResponseQueryField:
            pass
            
        rv            = ExecuteQueryResponse()
        rv.id         = tmp.id
        rv.queryField = []

        if not hasattr(self, '_queries'): self._queries = {}
        self._queries[tmp.id] = { 'queryId' : queryId, 'queryName': queryName, 'queryArgs': queryArgs }
        
        fields = sorted(tmp.queryField, key=lambda rec: int(re.search(r'(\d+)$', rec.fieldKey).group(1)))
        
        for field in fields:
            tmp = ExecuteQueryResponseQueryField()
            tmp.fieldKey = field.fieldKey
            tmp.fieldName = field.fieldName
            tmp.setName = field.setName
            if hasattr(field, 'displayFieldKey'): tmp.displayFieldKey = field.displayFieldKey
            rv.queryField.append(tmp)
            
        return rv
            
    def discardQuery(self, queryId):
        '''discardQuery(queryId)
        
        This releases a query instance and the resources associated with it. This is automatically
        performed by Labmatrix when logout() is called, or if the session times out.
        
        Args:
           queryId (int) : query identifier as returned by executeQuery()
           
        Returns:
           True if successful, otherwise False
        '''
        
        if queryId is not None:
            return self._server.DiscardQueryRequest(sessionToken = self._sessionToken, queryId = queryId)
        else:
            return None
            
    def retrieveResults(self, id, order = None, pageSize = 500, startIndex=0):
        '''retrieveResults(id, order = None, pageSize = 500, startIndex = 0)
        
        This function fetches query results from the Labmatrix SOAP web service. It requires the query result
        ID that is returned from executeQuery(). Note that this function may never return for queries that
        have a large number of results. It has been observed that in a data set returning 43 columns and
        540,000 rows, calls to retrieveResults() fail after retrieving 5000-6000 rows. This is presumed to
        be an internal memory/caching problem with the service itself. Your mileage may vary.
        
        Because of the issues with stability of the SOAP service, this call implements a timeout for
        retrying the SOAP call. If the call times out, it will logout(), login(), executeQuery(), and
        continue at the last startIndex.
        
        The intent of this function is to retrieve batches of up to 500 records. You can select subsequent
        records in the query by calling retrieveResults() with an approriately incremented startIndex.
        
        If you need to retreive all rows of a query that have no variables, then it's better to use the
        REST web service instead as it is more reliable and several orders of magnitude faster that the
        SOAP web service.
        
        It should be noted that this function will return the fields arranged in the same order as was
        returned from executeQuery(). This behavior differs from the SOAP web-service, which returns
        the fields in random order. The fields are sorted by the field key value.
        
        Args:
          id (int) : the query result id returned as part of the the response from executeQuery()
          order (string, optional) : the column used to sort the response
          pageSize (int, optional) : the maximum number of records to return; max is 500, numbers
            over 500 are assumed to be 500
          startIndex (int, optional) : start returning records from indicated row
          
        Returns:
          [ 'startIndex' : 1, 'endIndex': 500, 'pageSize': 500, 'totalRecordCount': 543210, 
            'executionTimeMillis': 10, 'record': [ ('key': 'key1', 'value': 'value1' ), ... ]]
        '''
        print 'retrieveResults({0}, {1}, {2}, {3})'.format(id, order, pageSize, startIndex)
        
        class RetrieveResultsTimeout(Exception):
            pass
            
        def timeouthandler(signum, frame):
            raise RetrieveResultsTimeout()
        
        signal.signal(signal.SIGALRM, timeouthandler)
        signal.alarm(180) # Wait up to 3 minutes for a response
        
        try:
            if order is None:
                result = self._server.RetrieveResultsRequest(sessionToken = self._sessionToken, id = id, pageSize = pageSize, startIndex = startIndex)
            else:
                result = self._server.RetrieveResultsRequest(sessionToken = self._sessionToken, id = id, pageSize = pageSize, startIndex = startIndex, order = order)
        except RetrieveResultsTimeout:
            print "# retrieve timed out, logging in again and retrying"
            self.logout()
            self.login(self._username, self._password, timeoutInMinutes = self._timeout)
            tmp = self._queries[id];
            del self._queries[id]
            tmp = self.executeQuery(tmp['queryId'], tmp['queryName'], **tmp['queryArgs'])
            return self.retrieveResults(tmp.id, order, pageSize, startIndex)
        finally:
            signal.alarm(0)
            
        class RetrieveResultsResponse:
            pass
            
        class RetrieveResultsResponseRecord:
            pass

        class RetrieveResultsResponseRecordFieldValue:
            pass
            
        rv = RetrieveResultsResponse()
        rv.endIndex             = result.endIndex
        rv.pageSize             = result.pageSize
        rv.executionTimeMillis  = result.executionTimeMillis
        rv.totalRecordCount     = result.totalRecordCount
        rv.startIndex           = result.startIndex
        rv.record               = []
        
        for rec in result.record:
            aRecord = RetrieveResultsResponseRecord()
            aRecord.fieldValue = []
            
            tmp = sorted(rec.fieldValue, key=lambda fv: int(re.search(r'(\d+)', fv.key).group(1)))
            for f in tmp:
                aFieldValue = RetrieveResultsResponseRecordFieldValue()
                aFieldValue.key     = f.key
                aFieldValue.value   = f.value if hasattr(f, 'value') else ''
                aRecord.fieldValue.append(aFieldValue)
                
            rv.record.append(aRecord)
            
        return rv
            
    def getResultsIterator(self, id, order = None):
        '''getResultsIterator(id, order = None)
        
        While restrieveResults() is a faithful presentation of the SOAP interface, it is a poor fit
        to the Python idiom. This function returns an iterator that calls on retrieveResults(). It
        retrieves and buffers 500 rows at a time to minimize the number of calls to the service.
        
        Args:
          id (int) : the query result id returned as part of the the response from executeQuery()
          order (string, optional) : the column used to sort the response

        Returns:
          ResultsIterator (each item is a result row)
        '''
        class ResultsIterator:
            def __init__(self, lmObject, id, order):
                self._lmObject = lmObject
                self._id       = id
                self._order    = order
                self._pageSize = 500
                
            def __iter__(self):
                return self
                
            def next(self):
                if not hasattr(self, '_len'):
                    self._r   = self._lmObject.retrieveResults(self._id, self._order, pageSize = self._pageSize)
                    self._len = self._r.totalRecordCount
                    self._n   = 0
                    if self._len < 1:
                        raise StopIteration
                    else:
                        return self._r.record[self._n]
                else:
                    self._n += 1
                    
                    print "### ", self._n
                    
                    if self._n >= self._len:
                        raise StopIteration
                        
                    if self._n % self._pageSize == 0:
                        self._r = self._lmObject.retrieveResults(self._id, self._order, pageSize = self._pageSize, startIndex = self._n)
                    
                    return self._r.record[self._n % self._pageSize]
        
        return ResultsIterator(self, id, order)
        
    def ETL(self, **kwArgs):
        '''ETL(data = None, dataFilePath = None, etlMappingCode = None, escapeChar = None, quoteChar = None, separatorChar = None)
        
        Attempt to load data by invoking one of the ETL processes stored in the LabMatrix application.
        
        Args:
          data (string, optional): a string containing the data to load; one of 'data' or 'dataFilePath' is required
          dataFilePath (string, optional): the path to a file containing the data to load; one of 'data' or
              'dataFilePath' is required
          etlMappingCode (string) : the identifier for the ETL process to be used
          escapeChar (string, optional) : default '\'; the string used to escape quotes
          quoteChar (string, optional) : default '\"'; the string used to quote columns
          separatorChar (string, optional) : default '\t'; string that is used to separate columns
           
        Return:
          True on success, False on failure.
        '''
        validArgs = [ 'data', 'dataFilePath', 'etlMappingCode', 'escapeChar', 'quoteChar', 'separatorChar' ]
        args = dict( (k,v) for k,v in kwArgs.iteritems() if k in  validArgs)
        
        if 'dataFilePath' in args:
            with open(args['dataFilePath'], 'r') as myData:
                args['data'] = myData.read()
                del args['dataFilePath']
        
        return self._server.ETLRequest(sessionToken = self._sessionToken, **args)
        
    def uploadTextAsDataSource(self, **kwArgs):
        '''uploadTextAsDataSource(data = None, dataFilePath = None, dataSourceName = None, dataSourceId = None,
            dataSourceAccessGroupExternalId = None, allowDatatypeChanges = False, autoDetectDataTypes = True,
            biomaterialExternalIdColumn = None, biomaterialExternalIdProperty = None, biomaterialExternalSource = None,
            biomaterialExternalSourceColumn = None, catalogAllStringFields = True, dataSourceComments = None,
            deleteData = None, deleteFilePath = None, escapeChar = None, externalIdColumn = None, externalSource = None,
            externalSourceColumn = None, failOnFirstError = True, numberOfLinesUsedToDeriveDatatypes = 10000,
            quoteChar = None, replaceAllData = False, replaceAllDataForEachSubjectOrBiomaterial = False,
            separatorChar = None, showDetailedErrors = False, subjectExternalIdColumn = None,
            subjectExternalIdProperty = None, subjectExternalSource = None, subjectExternalSourceColumn = None)
        
        Attempt to load data by invoking one of the ETL processes stored in the LabMatrix application.
        
        Args:
          data (string, optional): a string containing the data to load; one of 'data' or 'dataFilePath' is required
          dataFilePath (string, optional): the path to a file containing the data to load; one of 'data' or
              'dataFilePath' is required
          dataSourceId (int, optional): The Labmatrix ID of an existing datasource. Use this instead of the
              "dataSourceName" parameter to load data into an existing datasource.
          dataSourceName (string): The name of the datasource into which data will be uploaded. If this parameter does not match
              the name of an existing datasource, a new datasource will be created.
          dataSourceAccessGroupExternalId (string): Required for new datasources. The external ID of an Access Group in
              which to place the new datasource. This parameter only applies to new datasources.
          allowDatatypeChanges (boolean, optional): Only applicable when uploading data into an existing datasource. Setting
              this parameter to true allows Labmatrix to automatically adjust field datatype to the data being uploaded.
              For example, if you try to upload non-integer data values into an existing integer field, Labmatrix will
              change that field's datatype to text.
          autoDetectDataTypes (boolean, optional): If false, the second row in the file (the row below the column headers)
              will be used to specify the datatype of the column. Possible values are:
                  auto
                  boolean
                  integer
                  decimal
                  date
                  datetime
                  time
                  text
              Any value not recognized will default to "auto." If the value is "auto," Labmatrix will attempt to autodetect
              the datatype of the column.
          biomaterialExternalIdColumn (string, optional): The name of the column used to identify Biomaterial records.
          biomaterialExternalIdProperty (string, optional): The name of the API property on net.labmatrix.beans.Biomaterial
              against which values in the biomaterialExternalIdColumn are matched to identify Biomaterial records. We suggest
              using one of the following:
                  externalId
                  id
                  name
                  externalBarcode
                  guid (used as the system generated barcode)
          biomaterialExternalSource (string, optional): The external source name of the original system from which the
              biomaterial records referenced in the data file were loaded. The same external source is used for each row in
              the data file. To specify a different external source value for each row, see biomaterialExternalSourceColumn below.
          biomaterialExternalSourceColumn (string, optional): The name of a column containing the external source value for the
              biomaterial referenced in each row. See biomaterialExternalSource above for further details.
          catalogAllStringFields (boolean, optional): If true, each value in all string fields will be added to the Qiagram
              catalog. Cataloging can increase the time it takes to load large data.
          dataSourceComments (string, optional): If uploading a new datasource, this parameter specifies the comments to be
              applied to the datasource. These comments are searchable in Qiagram. This parameter only applies to new datasource.
          deleteData (string, optional): Tab delimited text containing records that should be deleted. This parameter is only
              applicable when loading data into an existing datasource. If you simply need to remove all existing data from
              the datasource before uploading new data, use the replaceAllData parameter.
          deleteFilePath (string, optional): Fully qualified path to the data file containing records that should be
              deleted from the datasource. This method is recommended when deleting large amounts of records. For small
              data loads, you can optionally use the !deleteData parameter to pass data in as a string. If you simply need
              to remove all existing data from the datasource before uploading new data, use the replaceAllData parameter.
          escapeChar (string, optional): Character to escape quote and separator characters. Defaults to "\".
          externalIdColumn (string, optional): Column containing record ids from the original source system. If specified,
              the original record ids can be used to update the records in the data file during subsequent uploads.
          externalSource (string, optional): The name of the source system from which the data originated. External Source
              is used in conjunction with External ID to uniquely identify a record within Labmatrix by its ID in the original
              system. This allows you to continue using a legacy identifier scheme even after data has been loaded into
              Labmatrix. If this parameter is specified, the same external source value is used for each row in the data
              file. Alternatively, you can specify a column containing the external source values for each row using the
              externalSourceColumn parameter below.
          externalSourceColumn (string, optional): The name of a column containing external source values for each row in
              the data file. See externalSource above for further description.
          failOnFirstError (boolean): If true, the system will report an error immediately after the first problem
              is found. If false, the system will try to continue loading data to find all of the errors in the file before
              reporting errors. In either case, data will not be saved if even a single error occurs.
          numberOfLinesUsedToDeriveDatatypes (int): The number of lines of data to inspect when determining a field's
              datatype. If left blank, all lines will be inspected. This parameter can be useful in speeding up the
              performance of data loads, by assuming that the data at the beginning of the file is representative of the
              data throughout the file.
          quoteChar (string, optional): Character to use when encapsulating a column value that contains the separator
              character. Defaults to the double-quote character (").
          replaceAllData (boolean):  If true, all existing data in the datasource will be deleted before new data is uploaded.
          replaceAllDataForEachSubjectOrBiomaterial (boolean): If true, all existing data in the datasource for only those
              subjects or biomtarials referenced in the data file will be deleted before new data is uploaded.
          separatorChar (string, optional): Character to use as the delimiter. Defaults to the tab character.
          showDetailedErrors (boolean): Set this flag to true to see more detailed error messages when uploads fail.
              When true, Labmatrix will use ETL to process the data. When false, Labmatrix will use SQL BULK INSERT.
              As a result, setting this flag to false will greatly increase performance and is recommended when the
              data is reasonably clean or contains more than 10,000 records.
          subjectExternalIdColumn (string, optional): The name of the column used to identify Subject records.
          subjectExternalIdProperty (string, optional): The name of the API property on net.labmatrix.beans.Subject against
              which values in the subjectExternalIdColumn are matched to identify Subject records. We suggest using one of the following:
                  externalId
                  id
                  customId
                  code
                  externalBarcode
                  internalBarcode
          subjectExternalSource (string, optional): The external source name of the original system from which the subject
              records referenced in the data file were loaded. The same external source is used for each row in the data file.
              To specify a different external source value for each row, see subjectExternalSourceColumn below.
          subjectExternalSourceColumn (string, optional): The name of a column containing the external source value for
              the subject referenced in each row. See subjectExternalSource above for further details.
           
        Return:
          DataSourceId success, False on failure.
        '''
        
        validArgs = [
            'allowDatatypeChanges', 'autoDetectDataTypes', 'data', 'dataFilePath',
            'biomaterialExternalIdColumn', 'biomaterialExternalIdProperty', 'biomaterialExternalSource', 'biomaterialExternalSourceColumn',
            'catalogAllStringFields', 'dataSourceAccessGroupExternalId', 'dataSourceId', 'dataSourceComments', 'dataSourceName',
            'deleteData', 'deleteFilePath', 'escapeChar', 'externalIdColumn', 'externalSource', 'externalSourceColumn',
            'failOnFirstError', 'numberOfLinesUsedToDeriveDatatypes', 'quoteChar', 'replaceAllData', 'replaceAllDataForEachSubjectOrBiomaterial',
            'separatorChar', 'showDetailedErrors', 'subjectExternalIdColumn', 'subjectExternalIdProperty', 'subjectExternalSource',
            'subjectExternalSourceColumn'
        ]
        args = dict( (k,v) for k,v in kwArgs.iteritems() if k in validArgs )
        
        if 'dataFilePath' in args:
            with open(args['dataFilePath'], 'r') as myData:
                args['data'] = myData.read()
                del args['dataFilePath']
                                 
        return self._server.UploadTextAsDataSourceRequest(sessionToken = self._sessionToken, **args)
        


'''An example.'''
if __name__ == "__main__":
    # Create a Labmatrix SOAP web service proxy
    lm = LabmatrixClient()
    username = None  # set the username
    password = None  # set the password
    
    # Login
    print lm.login( username, password )
    print lm.ping()
    
    print lm.uploadTextAsDataSource( dataFilePath = 'my.data',
                                     dataSourceName = 'Test Data',
                                     dataSourceAccessGroupExternalId = 'Test',
                                     autoDetectDataTypes = False )

    '''    
    # Execute query 54 - in our system, that's a query that returns all of the biomaterials
    response = lm.executeQuery(54)
    
    # Open a file to store the results
    labmatrix = open('labmatrix.txt', 'w')
    
    # write a header line, using the fieldNames from the queryFields
    labmatrix.write(u"\t".join([f.fieldName for f in response.queryField]).encode('utf-8'))
    labmatrix.write(u"\n")

    try:
        # iterator over the rows in the query result
        for record in lm.getResultsIterator(response.id):
            labmatrix.write(u"\t".join([ x.value for x in record.fieldValue ]).encode('utf-8'))
            labmatrix.write(u"\n")
    except KeyboardInterrupt:
        lm.logout()
    finally:
        labmatrix.close()
    '''
        
    lm.logout()
